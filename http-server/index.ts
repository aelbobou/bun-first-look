Bun.serve({
    port: 3000,
    fetch(_req: Request) {
        const body = JSON.stringify({ message: "Hello, World!" });
        const res = new Response(body, {
            status: 200,
            statusText: "OK",
            headers: {
                "Content-Type": "application/json",
            },
        });
        return res;
    },
  });