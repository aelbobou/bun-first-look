# Bun - First Look

## What is it?
- Bun is a js toolkit, which means it does the following and more:
	- Bun is a js runtime like node, deno, hermes
		- a runtime interprets and executes code
	- Bun is a package manager, like npm, yarm, pnpm
	- Bun is intended to be a drop-in replacement for node
- Bun is still in development, so not ready for production use

## Why a new runtime?
- Bun focuses on three things
	- Speed
	- Elegant APIs:
		- If you've used node standard library to write/read files, create an http server, you know how convoluted these apis are.
    - Cohesive Developer Experience
- No need to worry about different module formats like ESM, CommonJS, everything just works as intended.
- Typescript is a first class citizen

## Installation
- https://bun.sh/docs/installation

## Exploring a few key Bun APIs:
- http-server/
- file-io/ (not implemented)
- testing/ (not implemented)
- sqlite/ (not implemented)